# BookMe

* ¿Nunca has tenido que llamar a varios restaurantes antes de encontrar uno que te guste y con sitios libres donde reservar?
Pues hemos solucionado esto centralizando y creando un directorio de restaurantes para facilitar la reserva y poder conocer el aforo, horario, carta, etc.
En definitiva hemos creado un sistema donde facilitaremos la interacción cliente restaurante haciéndola rápida y eficaz, donde ambas partes salen ganando.

* Have you ever had to call various restaurants until you found one you liked with empty tables?
We have solved this by centralizing and creating a restaurants directory to make booking easy, find out about sitting capacity, timetable, restaurants menu, etc.
Overall we have created a system making client - restaurant interacction easy, fast and efficient where both parts come out winning.

#### Ramas / Branches:
* [**Front**](https://gitlab.com/PauAbellaMolina/bookme/-/tree/IonicFront)
* [**API**](https://gitlab.com/PauAbellaMolina/bookme/-/tree/API)
* [**Design Moqups**](https://gitlab.com/PauAbellaMolina/bookme/-/tree/Design)
* [**Database Diagram (primera versión)**](https://gitlab.com/PauAbellaMolina/bookme/-/tree/Database)

##### Tecnología usada / Technology used:
* **Front**: Ionic Framework for Angular, Bootstrap, Html, Scss, JQuery.
* **API**: Spring Boot Framework (Java).
* **Database**: MySQL server hosted by Fedora 32 OS.

##### Metodología de trabajo / Work methodology:
* Agile - Scrum

##### Herramientas más usadas / Tools used:
* VSCode
* Spring Tool Suite
* MySQL Workbench
* Postman
* VirtualBox
* GitLab
* Trello

##### Sobre nosotros / About us:
* Pau Abella - [LinkedIn](https://linkedin.pauabella.dev) - [GitHub](https://github.pauabella.dev) - [pauabella.dev](https://pauabella.dev)
* Alberto García - [LinkedIn](https://www.linkedin.com/in/alberto-garcia-6b9211151/)
* Aitor Alarcón - [LinkedIn](https://www.linkedin.com/in/aitor-alarc%C3%B3n-doblado-136212151/)